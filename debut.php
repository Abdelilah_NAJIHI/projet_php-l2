
<!DOCTYPE html>
<html>
<head>
	<title>Project Php</title>
	<style type="text/css">
nav {
   margin: 0;
}



nav ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    width: 25%;
    background-color: #DEB887;
    position: fixed;
    height: 100%;
    overflow: auto;
}

 nav li a {
    display: block;
    color: #000;
    padding: 8px 16px;
    text-decoration: none;
}

nav li a.active {
    background-color: #FFDEAD;
    color: white;
}

nav li a:hover:not(.active) {
    background-color: #555;
    color: white;
}
    
</style>
</head>
<body style="background-color:white;">

<nav>
