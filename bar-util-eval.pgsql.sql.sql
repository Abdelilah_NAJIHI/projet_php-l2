--
-- Structure de la table `Bar`(bar_id,bar_nom)
--
 
CREATE TABLE bar(
bar_id SERIAL, 
bar_nom VARCHAR(50) NOT NULL,
PRIMARY KEY (bar_id)
);
--
-- STRUCTURE DE LA TABLE `utilisateur`(util_id,util_pseudo)
--

CREATE TABLE utilisateur(
	util_id SERIAL,	
	util_pseudo VARCHAR(50) NOT NULL,
	PRIMARY KEY (util_id)
);
-- STRUCTURE DE LA TABLE `evaluation`

CREATE TABLE eval(
	   eval_id SERIAL,
       bar_id INTEGER ,
       util_id INTEGER,
       age INT NOT NULL,
       FOREIGN KEY (util_id) REFERENCES utilisateur(util_id),
       FOREIGN KEY (bar_id) REFERENCES utilisateur(bar_id),
       PRIMARY KEY (util_id,bar_id),
       eval_com VARCHAR(280) ,
       eval_note INTEGER CHECK (eval_note BETWEEN 1 AND 5) 
       
); 
--
-- Contenu de la table `bar`
--

INSERT INTO bar (bar_nom) VALUES

('Play To Win'),
('Le Trappist'),
('Mc Daid s'),
('Black Café'),
('Low Mana Gaming'),
('Le Décalé'),
('L Etable'),
('La Boiserie'),
('Le Bistrot'),
('Le New bar'),
('Waka Waka'),
('Bar La Chapelle'),
('Camp Gourou'),
('Au Coup Franc '),
('Les Champs Elysées'),
('Le backstage bar'),
('L Embuscade')
('Le chiquito'),
('L Eau Tarie '),
('Le Square');



--
-- CONTENU DE LA TABLE Utilisateur
--

INSERT INTO utilisateur (util_pseudo) VALUES
('Jeff'),
('Paulo'),
('Dony'),
('Kamil'),
('micheal'),
('STD2A'),
('ST2S'),
('STI2D'),
('STAV'),
('TMD'),
('TMD'),
('TMD'),
('TMD'),
('TMD'),
('TMD'),
('TMD'),
('HOTELLERIE OU STHR');



INSERT INTO eval(eval_id,bar_id, util_id, age, eval_note, eval_com) VALUES
	(24,8,8,'19','3','Meduim'),
	(25,10,6,'23','1','Very Bad'),
	(26,4,15,'21','4','Ambiance chouette, Large Choix des boissons et des plats et pas trop bondé.'),
	(27,8,14;'22','2','pas mal bar'),
	(28,14,19,'25','4','Facile de le trouver'),
	(29,1,23,'20','2','Bad'),
    (30,17,18,'19','4','coool'),
    (31,18,24,'25','4','Une ambiance de ouuuf'),
    (32,19,23,'20','4','parfait pour organiser une soirée'),
    (33,21,17,'21','1','une catastrophe'),
    (34,22,25,'5','5','Parfait, j adore'),
    (35,23,20,'25','4','bar trés sympathique avec un trés bon accueil'),
    (36,24,27,'22','3','un bar super sympa et décontracté'),
    (37,25,16,'28','1','Non recommandable'),
    (38,26,21,'18','4','Cadre agréable, personnel souriant aimable et efficace. Prix trés correct comparé au restaurant alentour.');


	
	